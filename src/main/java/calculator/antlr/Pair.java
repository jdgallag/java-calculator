package calculator.antlr;

/**
 * This is used to store two values in a pair
 * @param <FirstType> the type corresponding to the first element of the pair
 * @param <SecondType> the type corresponding to the second element of the pair
 */
public class Pair<FirstType, SecondType> {
    private FirstType first;
    private SecondType second;

    /**
     * 
     * @param one the first element of the pair
     * @param two the second element of the pair
     */
    public Pair(FirstType one,SecondType two){
        this.first = one;
        this.second = two;
    }

    public FirstType first(){
        return this.first;
    }

    public SecondType second(){
        return this.second;
    }

}