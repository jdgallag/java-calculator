package calculator.antlr;

/**
 * A class for calculator specific exceptions.  Could be extended to 
 * hold more information.
 */
public class CalculatorException extends Exception {
    public CalculatorException(){
        super();
    }

    public CalculatorException(String errorMessage){
        super(errorMessage);
    }
}

