grammar Calculator;

// force a complete consumption
compileUnit
    :   expr EOF
    ;

// Warning: order matters for infix operations
// The order here is by operation order.
// If e.g. + is put above * then 
// The parser will render 1+1*2 as (1+1)*2
expr:	
    '(' expr ')'  # Braces
    
    | expr EBINFIXOP expr # InfixBinary
    | expr '^' expr # Expon 
    | expr '*' expr # Mult 
    | expr '/' expr # Div
    | expr '+' expr # Add 
    
    | expr '-' '(' expr ')'  # Sub
    // to mix subtraction with the strict definition of numbers used here, we would have to allow -0 to pass parsing
    // Generally, we would allow OPNAME to direct operations, but lets, are special because they require the first argument to be a variable, i.e. they are special 
    | 'let' '(' IDENT ',' expr ',' expr ')' # Let
    | IDENT '(' (expr  (',' expr)*) ')' # OpName
    | IDENT # Ident
    | INT          # Int
    // this is used to catch int lists ... which we explicitly disallow.
    | INT* # ListInt

    
    // | '-' expr  # NegExpression
    ;




// Here one can make extensible binary operations.  By default these bind most strongly,
// So 1+2#2 will be parsed as though it it 1+(2#2).  Once added here, one must 
// add an additional case in the CalculatorVisitor in the switch to handle the new 
// operation.
EBINFIXOP : '#'  ; 


IDENT : ([a-zA-Z] |UNDERSCORE)+([a-zA-Z0-9]|UNDERSCORE)*;
UNDERSCORE : '_' ;


WS : (' ' | '\t')+ -> channel(HIDDEN);

// INT :  '0' | [1-9] [0-9]*;

INT     : '0' | ('-'? [1-9]) ([0-9]*)  ;
// integer : 
//     '0' # ZERO
//     | INT # PosInt
//     | '-' INT # NegInt;

// INT : [1-9] [0-9]* ;
