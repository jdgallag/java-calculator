package calculator.antlr;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

//     private Calculator calc = new Calculator();
    private ParseCheckEval calculator = new ParseCheckEval();
    
    /*
      Test that the simplest constant returns the right thing
    */
    @Test
    public void constantTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("3");
            assertEquals(Integer.valueOf(3),m);
        }
        catch(Exception e){
            Assert.fail("single digits should pass");
        }
    }

    /*
      Test that a simple ident declared with a let returns the right thing
    */
    @Test
    public void simpleIdentTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("let(x,1,x)");
            assertEquals(Integer.valueOf(1),m);
        }
        catch(Exception e){
            Assert.fail("simple declared variables should pass");
        }
    }

    /*
        Test that a parenthetized expression works
    */
    @Test
    public void parenTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("(((4)))");
            assertEquals(Integer.valueOf(4),m);
        }
        catch(Exception e){
            Assert.fail("parenthetized expressions should pass");
        }
    }

    /*
        Test that custom infix binary works
    */
    @Test
    public void customInfixTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("3#2");
            assertEquals(Integer.valueOf(3*3+2), m);
        }
        catch(Exception e){
            Assert.fail("custom infix should work");
        }
    }

    /*
        Test infix addition
    */
    @Test
    public void infixPlus(){
        try{
            Integer m = calculator.parseCheckEvaluate("3+5");
            assertEquals(Integer.valueOf(8), m);
        }
        catch(Exception e){
            Assert.fail("infix addition should work");
        }
    }

    /*
        Test infix subtraction
    */
    @Test
    public void infixMinusTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("5-(3)");
            assertEquals(Integer.valueOf(2),m);
        }
        catch(Exception e){
            Assert.fail("infix subtraction should work");
        }
    }

    /*
        Test 0 works
    */
    @Test 
    public void zeroTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("0");
            assertEquals(Integer.valueOf(0),m);
        }
        catch(Exception e){
            Assert.fail("0 should pass");
        }
    }

    /*
        Test negative numbers work
    */
    @Test
    public void negativeNumbersTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("-3");
            assertEquals(Integer.valueOf(-3),m);
        }
        catch(Exception e){
            Assert.fail("negative numbers should pass");
        }
    }

    /*
        Test multiplication works
    */
    @Test
    public void multTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("3*4");
            assertEquals(Integer.valueOf(12),m);
        }
        catch(Exception e){
            Assert.fail("multiplication of numbers should work");
        }
    }

    /*
        Test division works
    */
    @Test
    public void divisionTest(){
            try{
                Integer m = calculator.parseCheckEvaluate("8/4");
                assertEquals(Integer.valueOf(2),m);
            }
            catch(Exception e){
                Assert.fail("division should work");
            }
    }

    /*
        Test exponentiation works
    */
    @Test
    public void exponTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("2^3");
            assertEquals(Integer.valueOf(8),m);
        }
        catch(Exception e){
            Assert.fail("exponentiation should work");
        }
    }

    /*
        Simple let test
    */
    @Test 
    public void letTest1(){
        try{
            Integer m = calculator.parseCheckEvaluate("let(x,3,add(x,4))");
            assertEquals(Integer.valueOf(7),m);
        }
        catch(Exception e){
            Assert.fail("let(x,3,add(x,4)) should work");
        }
    }

    /*
        Simple operation add test
    */
    @Test
    public void operationAddTest1(){
        try{
            Integer m = calculator.parseCheckEvaluate("add(1,2)");
            assertEquals(Integer.valueOf(3),m);
        }
        catch(Exception e){
            Assert.fail("simple addition operation should work");
        }
    }

    /*
        Simple operation sub test
    */
    @Test
    public void operationSubTest1(){
        try{
            Integer m = calculator.parseCheckEvaluate("sub(1,2)");
            assertEquals(Integer.valueOf(-1),m);
        }
        catch(Exception e){
            Assert.fail("simple subtraction operation should work");
        }
    }

    /*
        Simple operation mul test
    */
    @Test
    public void operationMulTest1(){
        try{
            Integer m = calculator.parseCheckEvaluate("mul(1,2)");
            assertEquals(Integer.valueOf(2),m);
        }
        catch(Exception e){
            Assert.fail("simple multiplication operation should work");
        }
    }

    /*
        Simple operation div test
    */
    @Test
    public void operationDivTest1(){
        try{
            Integer m = calculator.parseCheckEvaluate("div(4,2)");
            assertEquals(Integer.valueOf(2),m);
        }
        catch(Exception e){
            Assert.fail("simple division operation should work");
        }
    }

    /*
        Parallel let works
    */
    @Test
    public void parallelLetTest1(){
        try{
            Integer m = calculator.parseCheckEvaluate("add(let(x,1,x),let(x,1,x))");
            assertEquals(Integer.valueOf(2),m);
        }
        catch(Exception e){
            Assert.fail("parallel lets with the same variable should work");
        }
    }

    /*
        A stacked term computing the 5th fibonacci number:
        fib0 = 0; 
        fib1 = 1; 
        fib2 = fib0+fib1;  
        fib3 = add(fib1,fib2); 
        fib4 = add(fib2,fib3); 
        fib5 = add(fib3,fib4); 
        return fib5; 
    */  
    @Test
    public void fib5Test(){
        try{
            String fib5 = "let(fib5,add(fib3,fib4),fib5)";
            String fib4 = "let(fib4,add(fib2,fib3)," + fib5 + ")";
            String fib3 = "let(fib3,add(fib1,fib2)," + fib4 + ")";
            String fib2 = "let(fib2,fib0+fib1," + fib3 +")";
            String fib1 = "let(fib1,1," + fib2 + ")";
            String fib0 = "let(fib0,0," + fib1 + ")";
            Integer m = calculator.parseCheckEvaluate(fib0);
            assertEquals(Integer.valueOf(5),m);
        }
        catch(Exception e){
            Assert.fail("stacking lets to make fibonacci should work");
        }
    }

    /*
        Spacing should be ignored
    */
    @Test
    public void spacingIgnoreTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("add (div( 1   +    5,   2)  ,  3)");
            assertEquals(Integer.valueOf(6),m);
        }
        catch(Exception e){
            Assert.fail("spacing should be ignored");
        }
    }

    
    
    /*
        addn test
    */
    @Test
    public void addnTest(){
        try{
            //1+2+3+4 = 10
            //add(1,let(x,1,x)+2) -> add(1,1+2) -> 4
            //10+4+ = 184 
            Integer m = calculator.parseCheckEvaluate("addn(1,2,3,4) + add(1,let(x,1,x) + 2) + 2 ^ 2");
            assertEquals(Integer.valueOf(18),m);
        }
        catch(Exception e){
            Assert.fail("addn should work");
        }
    }

    //canonical tests

    /*
        Canonical test1
    */
    @Test
    public void canonicalTest1(){
        try{
            Integer m = calculator.parseCheckEvaluate("add(1,2)");
            assertEquals(Integer.valueOf(3),m);
        }
        catch(Exception e){
            Assert.fail("add should work");
        }
    }

    /*
        Canonical test 2
    */
    @Test
    public void canonicalTest2(){
        try{
            Integer m = calculator.parseCheckEvaluate("sub(111,-30000)");
            assertEquals(Integer.valueOf(30111),m);
        }
        catch(Exception e){
            Assert.fail("sub should work with negatives");
        }
    }

    /*
        Canonical test 3
    */
    @Test 
    public void canonicalTest3(){
        try{
            Integer m = calculator.parseCheckEvaluate("div(5,2)");
            assertEquals(Integer.valueOf(2),m); 
        }
        catch(Exception e){
            Assert.fail("div should work");
        }
    }

    /*
        Canonical test 4
    */
    @Test
    public void canonicalTest4(){
        try{
            Integer m = calculator.parseCheckEvaluate("add(1,mul(2,3))");
            assertEquals(Integer.valueOf(7),m);
        }
        catch(Exception e){
            Assert.fail("add and mul should work together");
        }
    }

    /*
        Canonical test 5
    */
    @Test 
    public void canonicalTest5(){
        try{
            Integer m = calculator.parseCheckEvaluate("mul(add(2, 2), div(9, 3)) ");
            assertEquals(Integer.valueOf(12),m);
        }
        catch(Exception e){
            Assert.fail("mul, add, and div should work together");
        }
    }

    /*
        Canonical test 6
    */
    @Test
    public void canonicalTest6(){
        try{
            Integer m = calculator.parseCheckEvaluate("let(a, 5, add(a, a))");
            assertEquals(Integer.valueOf(10), m);
        }
        catch(Exception e){
            Assert.fail("a bound variable can be used more than once");
        }
    }

    /*
        Canonical test 7
    */
    @Test 
    public void canonicalTest7(){
        try{
            //a=5;
            //b=10*a = 50
            //add(b,a)=50+5 = 55
            Integer m = calculator.parseCheckEvaluate("let(a, 5, let(b, mul(a, 10), add(b, a))) ");
            assertEquals(Integer.valueOf(55),m);
        }
        catch(Exception e){
            Assert.fail("lets can be stacked, and bound variables may be used in any order");
        }
    }

    /*
        Canonical test 8
    */
    @Test
    public void canonicalTest8(){
        try{
            //a = {b=10;return b+b;}; = 20
            //b = 20;
            //add(a,b) = 40
            Integer m = calculator.parseCheckEvaluate("let(a, let(b, 10, add(b, b)), let(b, 20, add(a, b))) ");
            assertEquals(Integer.valueOf(40),m);
        }
        catch(Exception e){
            Assert.fail("a single variable bound twice, but in parallel, is fine");
        }
    }

    /*
        Canonical test 8 variant
    */
    @Test
    public void canonicalTest8Riff1(){
        try{
            //{x=1;return x;} + {x=1;return x;} = 2
            Integer m = calculator.parseCheckEvaluate("add(let(x,1,x),let(x,1,x))");
            assertEquals(Integer.valueOf(2),m);
        }
        catch(Exception e){
            Assert.fail("parallel bindings of a variable are admissable");
        }
    }

    /*
        Variables cannot be rebound test
        x=1;
        x=2;
        return x;
    */
    @Test 
    public void variableReboundTest1(){
        try{
            Integer m = calculator.parseCheckEvaluate("let(x,1,let(x,2,x))");
            Assert.fail("variables cannot be rebound");
        }
        catch(Exception e){
            //we should be throwing an error here, so nothing to do!
        }
    }

    /*
        Variables cannot be rebound test2
    */
    @Test
    public void variableReboundTest2(){
        try{
            Integer m = calculator.parseCheckEvaluate("let(x,1,let(x,2,x)+x)");
            Assert.fail("variables cannot be rebound");
        }
        catch(Exception e){
            //We should be throwing an error here, so nothing to do!
        }
    }

    /*
        Test that undeclared variables fail
    */
    @Test 
    public void undeclaredVarTest1(){
        try{
            Integer m = calculator.parseCheckEvaluate("x");
            Assert.fail("variables must be declared before use");
        }
        catch(Exception e){
            //We should be throwing an error here, so nothing to do!
        }
    }

    /*
        Tests that undeclarted variables fail, even if they have been previously declared
        e.g.
        let(x,1,x) + x
    */
    @Test
    public void undeclaredVarTest2(){
        try{
            Integer m = calculator.parseCheckEvaluate("let(x,1,x)+x");
            Assert.fail("variables must be declared before use");
        }
        catch(Exception e){
            //We should fail, so nothing to do!
        }
    }

    /*
        If a variable is declared, then it must be used
    */
    @Test
    public void variableUseTest1(){
        try{
            Integer m = calculator.parseCheckEvaluate("let(x,1,2)");
            Assert.fail("variables that are declared, must be used");
        }
        catch(Exception e){
            //this is correct
        }
    }

    /*
        If a variable is declared, it must be used part 2
    */
    @Test
    public void variableUseTest2(){
        try{
            Integer m = calculator.parseCheckEvaluate("let(x,1,let(y,2,y))");
            Assert.fail("variables that are declared must be used");
        }
        catch(Exception e){
            //expected behaviour
        }
    }

    // Runtime check -- division by 0
    @Test 
    public void infixDiv0Test(){
        try{
            Integer m = calculator.parseCheckEvaluate("div(5,0)");
            Assert.fail("division by 0 should not be allowed");
        }
        catch(Exception e){
            //expected behaviour
        }
    }
    @Test 
    public void opDiv0Test(){
        try{
            Integer m = calculator.parseCheckEvaluate("5/0");
            Assert.fail("division by 0 should not be allowed");
        }
        catch(Exception e){
            //expected
        }
    }

    //Lexer and parser fails

    /*
        Nonsense symbol
    */
    @Test 
    public void lexAndParseError1(){
        try{
            Integer m = calculator.parseCheckEvaluate("$");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Nonsense symbol
    */
    @Test 
    public void lexAndParseError2(){
        try{
            Integer m = calculator.parseCheckEvaluate("@");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Nonsense symbol
    */
    @Test 
    public void lexAndParseError3(){
        try{
            Integer m = calculator.parseCheckEvaluate("$%");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Nonsense symbol
    */
    @Test 
    public void lexAndParseError4(){
        try{
            Integer m = calculator.parseCheckEvaluate("%#");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        -0 is disallowed
    */
    @Test 
    public void lexAndParseError5(){
        try{
            Integer m = calculator.parseCheckEvaluate("-0");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        # is an allowed operation, but it needs arguments!
    */
    @Test 
    public void lexAndParseError6(){
        try{
            Integer m = calculator.parseCheckEvaluate("#");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        # is an allowed operation, but it needs good arguments!
    */
    @Test 
    public void lexAndParseError7(){
        try{
            Integer m = calculator.parseCheckEvaluate("2#$");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Double commas aren't a thing
    */
    @Test 
    public void lexAndParseError8(){
        try{
            Integer m = calculator.parseCheckEvaluate("add(3,,4)");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        At least 1 comma is required
    */
    @Test 
    public void lexAndParseError12(){
        try{
            Integer m = calculator.parseCheckEvaluate("add(3 4)");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
      Right close brackets are required
    */
    @Test 
    public void lexAndParseError9(){
        try{
            Integer m = calculator.parseCheckEvaluate("add(3,4");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
      Left close brackets are required
    */
    @Test 
    public void lexAndParseError10(){
        try{
            Integer m = calculator.parseCheckEvaluate("add3,4)");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
      Operations need to have a non-empty name
    */
    @Test 
    public void lexAndParseError11(){
        try{
            Integer m = calculator.parseCheckEvaluate("(3,4)");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
      Semicolons are not commas!
    */
    @Test 
    public void lexAndParseError13(){
        try{
            Integer m = calculator.parseCheckEvaluate("(3;4)");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
      Arguments must be supplied
    */
    @Test 
    public void lexAndParseError14(){
        try{
            Integer m = calculator.parseCheckEvaluate("add( , 4)");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        In this language, subtraction requires the right argument to be braces wrapped
    */
    @Test 
    public void lexAndParseError15(){
        try{
            Integer m = calculator.parseCheckEvaluate("3-4");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        0 prefixes are not allowed
    */
    @Test 
    public void lexAndParseError16(){
        try{
            Integer m = calculator.parseCheckEvaluate("01");
            Assert.fail("nonsense string");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Function symbols used must actually be known
    */
    @Test
    public void undeclaredOperationTest(){
        try{
            Integer m = calculator.parseCheckEvaluate("hog(1,2)");
            Assert.fail("hog is not defined");
        }
        catch(Exception e){
            //expected
        }
    }

    //Arity checks

    /*
        Add cannot have 0 arguments
    */
    @Test
    public void addArity1(){
        try{
            Integer m = calculator.parseCheckEvaluate("add()");
            Assert.fail("add requires 2 args");
        }
        catch(Exception e){
            //expected
        }
    }
    /*
        Add cannot have 1 arguments
    */
    @Test
    public void addArity2(){
        try{
            Integer m = calculator.parseCheckEvaluate("add(1)");
            Assert.fail("add requires 2 args");
        }
        catch(Exception e){
            //expected
        }
    }
    /*
        Add cannot have 3 arguments
    */
    @Test
    public void addArity3(){
        try{
            Integer m = calculator.parseCheckEvaluate("add(1,2,3)");
            Assert.fail("add requires 2 args");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Sub cannot have 0 arguments
    */
    @Test
    public void subArity1(){
        try{
            Integer m = calculator.parseCheckEvaluate("sub()");
            Assert.fail("sub requires two args");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Sub cannot have 1 argument
    */
    @Test
    public void subArity2(){
        try{
            Integer m = calculator.parseCheckEvaluate("sub(1)");
            Assert.fail("sub requires two args");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Sub cannot have 3 arguments
    */
    @Test
    public void subArity3(){
        try{
            Integer m = calculator.parseCheckEvaluate("sub(1,2,3)");
            Assert.fail("sub requires two args");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Mul cannot have 0 arguments
    */
    @Test
    public void mulArity1(){
        try{
            Integer m = calculator.parseCheckEvaluate("mul()");
            Assert.fail("mul requires two args");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Mul cannot have 1 argument
    */
    @Test
    public void mulArity2(){
        try{
            Integer m = calculator.parseCheckEvaluate("mul(1)");
            Assert.fail("mul requires two args");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Mul cannot have 3 arguments
    */
    @Test
    public void mulArity3(){
        try{
            Integer m = calculator.parseCheckEvaluate("mul(1,2,3)");
            Assert.fail("mul requires two args");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Div cannot have 0 arguments
    */
    @Test
    public void divArity1(){
        try{
            Integer m = calculator.parseCheckEvaluate("div()");
            Assert.fail("div requires two args");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Div cannot have 1 argument
    */
    @Test
    public void divArity2(){
        try{
            Integer m = calculator.parseCheckEvaluate("div(1)");
            Assert.fail("div requires two args");
        }
        catch(Exception e){
            //expected
        }
    }

    /*
        Div cannot have 3 arguments
    */
    @Test
    public void divArity3(){
        try{
            Integer m = calculator.parseCheckEvaluate("div(1,2,3)");
            Assert.fail("div requires two args");
        }
        catch(Exception e){
            //expected
        }
    }


}
